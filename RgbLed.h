/*
  RgbLed.h - Library for RGB led control.
  Created by M. Wodarz, 2020-02-19.
  Released into the public domain.
*/
#ifndef RgbLed_h
#define RgbLed_h

#include "Arduino.h"

enum Color {RED, GREEN, BLUE, YELLOW, ORANGE, VIOLET, TURKIS,WHITE,BLACK,OFF};

class RgbLed {
	public:
		RgbLed(int _pinRed, int _pinGreen, int _pinBlue);
		void showColorSequence(int delayMs, Color colorSeq[],int seqLenth);
		void showColor(Color color);
		void showColor(int red, int green, int blue);
		
	private:
		// setting PWM properties
		const int freq = 5000;
		const int ledChannelR = 0;
		const int ledChannelG = 1;
		const int ledChannelB = 2;
		const int resolution = 8;
	   
		int pinRed;
		int pinGreen;
		int pinBlue;
};

#endif