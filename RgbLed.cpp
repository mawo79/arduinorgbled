#include "Arduino.h"
#include "RgbLed.h"

RgbLed::RgbLed(int _pinRed, int _pinGreen, int _pinBlue)    {
      pinRed=_pinRed;
      pinGreen=_pinGreen;
      pinBlue=_pinBlue;

      pinMode (pinRed, OUTPUT);
      pinMode (pinGreen, OUTPUT);
      pinMode (pinBlue, OUTPUT);

      ledcSetup(ledChannelR, freq, resolution);
      ledcSetup(ledChannelG, freq, resolution);
      ledcSetup(ledChannelB, freq, resolution);

      ledcAttachPin(pinRed, ledChannelR);
      ledcAttachPin(pinGreen, ledChannelG);
      ledcAttachPin(pinBlue, ledChannelB);
    }
	
void RgbLed::showColorSequence(int delayMs, Color colorSeq[],int seqLenth){
    // int seqLenth=sizeof(colorSeq)/sizeof(colorSeq[0]);
    for(int i = 0 ; i< seqLenth;i++){
      showColor(colorSeq[i]);
      delay(delayMs);
    }
  }

void RgbLed::showColor(Color color) {
    switch(color){
      case RED: showColor(255,0,0); break;
      case GREEN: showColor(0,255,0); break;
      case BLUE: showColor(0,0,255); break;
      case YELLOW: showColor(255,255,0); break;
      case ORANGE: showColor(255,70,0); break;
      case TURKIS: showColor(0,255,255); break;
      case VIOLET: showColor(255,0,255); break;
      case WHITE: showColor(255,255,255); break;
      case BLACK:
      case OFF: showColor(0,0,0); break;
      default: showColor(255,255,255); break;
    }
  }

void RgbLed::showColor(int red, int green, int blue){
    ledcWrite(ledChannelR, red);
    ledcWrite(ledChannelG, green);
    ledcWrite(ledChannelB, blue);
}